﻿set search_path to prueba;

create table Indicador(
id_indicador character varying(10),
indicador character varying(200)
);

create table Entidad(
 clave_entidad character varying(2),
 entidad character varying(25)
);

create table Municipio(
clave_entidad character varying(2),
clave_mun character varying(5),
municipio character varying(200)
);

create table Nivel(
 id_nivel character varying(2),
 nivel character varying(100)
);


create table Subnivel(
 id_nivel character varying(2),
 id_subnivel character varying(3),
 subnivel character varying(50)
);

create table Anio(
 id_anio character varying(2),
 anio character varying(4)
);


create table Tasa_poblacional(
 id_tasa character varying(5),
 id_anio character varying(2),
 id_identificador character varying(10),
 cantidad character varying(12),
 clave_mun character varying(2),
 id_subnivel character varying(3)
);